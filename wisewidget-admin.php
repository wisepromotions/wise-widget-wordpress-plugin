<?php



function wisewidget_dashboard() {
	add_menu_page('Wise Widget', 'Wise Widget', 'wisewidget_manage', 'wisewidget', "wise_admin_page","", '24');
}


/*
 * Wise Widget Admin Page
 */
function wise_admin_page() {
	$wise_config = get_option('wisewidget_config');
	
	if (isset($_POST['submit']) && $_POST['submit']=="register") {
		$code = base64_decode ( trim($_POST['wise-key']) , false );
		if ($code === false || $code == $_POST['wise-key']) {
			$error_code = "Invalid key, please try again and insure your copying/pasting the entire key or contact us for help";
		} else { 
			$wise_config['license'] = strtok($code,":");
			$wise_config['spot'] = strtok(":");
			$wise_config['blog']  = strtok(":");
			update_option('wisewidget_config', $wise_config);
		}
		
	} else if (isset($_POST['submit2']) && $_POST['submit2']=="unregister") {
		$wise_config['spot'] = "";
		$wise_config['license'] = "";
		$wise_config['blog']  = "";
		update_option('wisewidget_config', $wise_config);
	}
	?>
	<div class="wise-admin-wrap">
	  <div class="wise-admin-logo-bar" >
	  	 <img src="<?php echo plugins_url( 'theme/wise-widget-logo.svg',  __FILE__); ?>" class="logo" width="300"/>
	  	 <div class="wisewidget-admin-title">A different way to advertise</div>
		 <div class="wise-admn-tagline">by Wise Network, a SheSaved Inc. company</div>
	  </div>
	  <div class="wise-admin-content" >
	  	<h3>Using the Wise Widget</h3>
	  	<p>Simply as 1,2,3...</p>
	  	<ol>
	  	<li>The first thing to do is get your license key from Keri Lynn, this will allow you to setup the widget for use.</li>
	  	<li>Next, Take your license key and enter it in the License Activations box to the right.
	  	<li>Finally, simply go to the Widget page and add the Wise Widget to one of your content areas and start reaping the rewards of Wise Advertising</li>
	  	</ol>
	  
	  </div>
	  <div class="wise-admin-content" >
	      <h3>Wise Widget Activation</h3>
	      <form method="post">
	  	  <?php  if (strlen($wise_config['spot']) < 5): ?>
	  	  	<p><label>Enter your license key to activate the plugin:</label></p>
	  	  	<p><input type="text" name="wise-key" /></p>
	  	  	<p><input type="submit" name="submit" value="register"></p>
	  	  	
	  	  <?php else: ?>
	  	  	<h5>Plugin Activated</h5>
	  	  	<p><b>Registration Name:</b> <?php echo $wise_config['blog']; ?></p>
	  	  	<p><b>Spot Code:</b> <?php echo $wise_config['spot']; ?></p>
	  	  	<p><b>Publisher Code:</b> <?php echo $wise_config['license']; ?></p>
	  	  	<p><input type="submit" name="submit2" value="unregister"></p>
	  	  <?php endif; ?>
		  </form>
	  </div>
	  
	  <?php 
}