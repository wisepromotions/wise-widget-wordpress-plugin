<?php 

/*
 * Plugin Name: WiseWidget
 * Plugin URI: https://wiseadvertising.agency/wisewidget
 * Author: Wise Promotions
 * Author URI: https://wiseadvertising.agency/
 * Description: Wise Promotions, better deals, better revenue
 * Version: 1.0.1
 * License: GPLv2
 */


function wisewidget_activate() {
	$wise_config = get_option('wisewidget_config');
	if($config === false || !is_array($config)) {
		$blog = get_site_url();
		$config = array('license' => '','spot' => '', 'blog' => $blog);
		update_option('wisewidget_config', $config);
	}
	// Set the admin capabilities 
	$role = get_role('administrator');		
	$role->add_cap("wisewidget_manage");
}

function wisewidget_deactivate() {
	
}

function wisewidget_uninstall() {
	update_option('wisewidget_config', "");
}
 
 //House cleaning hooks
register_activation_hook(__FILE__, 'wisewidget_activate');
register_deactivation_hook(__FILE__, 'wisewidget_deactivate');
register_uninstall_hook(__FILE__, 'wisewidget_uninstall');

function wisewidget_scripts() {
	wp_enqueue_script( "wiseswidget-js", plugins_url( '/js/wisewidget.js' , __FILE__),array('jquery'), 1.0, true);
	wp_enqueue_style( 'wiseswidget-stylesheet', plugins_url( '/css/wisewidget.css', __FILE__ ) );
}

add_action( 'widgets_init', 'wise_widget_init' );
 
function wise_widget_init() {
    register_widget( 'wise_widget' );
}
 
class wise_widget extends WP_Widget {
 
    public function __construct()
    {
        $widget_details = array(
            'classname' => 'wise_widget',
            'description' => 'Wise Widget '
        );
 
        parent::__construct( 'wise_widget', 'Wise Widget', $widget_details );
 
    }
 
    public function form( $instance ) {
        // Backend Form
        $title = ! empty( $instance['title'] ) ? $instance['title'] : ""; 
        $theme = ! empty( $instance['theme'] ) ? $instance['theme'] : "dark";
        ?>
	  <p>
	    <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
	    <input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>" />
	  </p>
	  <p>
	  	<label for="<?php echo $this->get_field_id( 'theme' ); ?>" >Theme:</label>
	  	 <input type="radio" id="<?php echo $this->get_field_id( 'theme' ); ?>" name="<?php echo $this->get_field_name( 'theme' ); ?>"  value="dark" <?php echo ($theme=="dark" ? "checked":""); ?> /> Dark &nbsp; 
	  	 <input type="radio" id="<?php echo $this->get_field_id( 'theme' ); ?>" name="<?php echo $this->get_field_name( 'theme' ); ?>" value="light" <?php echo ($theme=="light" ? "checked":""); ?> /> light <br />
	  
	  <?php 

    }
 
    public function update( $new_instance, $old_instance ) {  
       $instance = array();
	   $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	   $instance['theme'] = ( ! empty( $new_instance['theme'] ) ) ? strip_tags( $new_instance['theme'] ) : '';
	   return $instance;
    }
 
    public function widget( $args, $instance ) {
    	$wise_config = get_option('wise_widget_config');
        // Frontend display HTML
        echo $args['before_widget'];
        
        $theme = $instance['theme'];
        if ( empty($theme) ) $theme = "dark";
		
		?>
		<div id="wise-widget-wrapper" class="<?php echo $theme; ?>">
			<?php if ( ! empty( $instance['title'] ) ) echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title']; ?>
			<div id="wise-widget-theme">
				<img src="<?php echo plugins_url( 'theme/wise-owl-' . $theme . '.svg',  __FILE__); ?>" id="wise-theme-image" />
			</div>
			<div id="wise-wdiget-ad">
				<div id='div-gpt-ad-wise-widget-0' >
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-wise-widget-0'); });
				</script>
				</div>
			</div>
		</div>
		<?php

		echo $args['after_widget'];
    }
 
}


if ( is_active_widget( false, false, 'wise_widget', true ) ) add_action('wp_head', 'wise_js_head');

function wise_js_head() {
	$wise_config = get_option('wisewidget_config');
	echo "<!--Wise Widget for {$wise_config['license']}-->\n";
	if (strlen($wise_config['license']) > 5) {

	   ?> 	
<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
    wise_slot = googletag.defineSlot('/<?php echo $wise_config['license']; ?>/<?php echo $wise_config['spot']; ?>', [300, 250], 'div-gpt-ad-wise-widget-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
    googletag.pubads().addEventListener('slotRenderEnded', function(event) {
	    if (event.slot === wise_slot && !event.isEmpty) {
	      jQuery("#wise-widget-wrapper").show();
	    }
	});
  });
</script>
	
		<?php
	}	
}

if(!is_admin()) {
	add_action( 'wp_enqueue_scripts', 'wisewidget_scripts');
} else {
	include_once(plugin_dir_path( __FILE__ ) . 'wisewidget-admin.php');
	add_action('admin_menu', 'wisewidget_dashboard');
	add_action('admin_enqueue_scripts', 'wisewidget_scripts' );
	require 'plugin_update_check.php';
	
	include_once(plugin_dir_path( __FILE__ ) . 'plugin_update_check.php');
	$MyUpdateChecker = new PluginUpdateChecker_2_0 (
	   'https://kernl.us/api/v1/updates/59a440a4d700015ffb69cbae /',
	   __FILE__,
	   'wisewidget',
	   1
	);
	// $MyUpdateChecker->purchaseCode = "somePurchaseCode"; 
	$MyUpdateChecker->remoteGetTimeout = 5; 
}



